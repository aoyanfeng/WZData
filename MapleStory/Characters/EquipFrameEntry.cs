﻿using System;
using System.Collections.Generic;
using System.Text;
using WZData.MapleStory.Images;
using WZData.MapleStory.Items;

namespace WZData.MapleStory.Characters
{
    public class EquipFrameEntry
    {
        public Equip Equip;
        public string Action;
        public string Position;
        public IFrame SelectedFrame;
    }
}
