using System;
using WZData;

public class SkillTree {
    public int JobId;
    public SkillDescription Description;
    public SkillDescription[] Skills;
}